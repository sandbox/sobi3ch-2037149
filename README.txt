/**
 * @file
 * README for uc_cc_fee_domain.
 */

Usage
=====
After enabling module you need to enable fees per domain individually. This can
be done by ticking checkbox under "Ubercart credit card fee settings" on each
domain settings page. After save, you should see default fees generated for you
on this domain. Then you can tweak them in the same way as global fees
settings.



